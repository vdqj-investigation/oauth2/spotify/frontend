import { Component } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'angular16';

  constructor(private authService: AuthService) {}

  ngOnInit() {
    // Revisar si estamos regresando de Spotify con un código.
    //this.authService.handleAuth();
  }

  login() {
    this.authService.login();
  }
}

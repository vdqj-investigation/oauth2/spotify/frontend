import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  private readonly TOKEN_KEY = 'accessToken';

  constructor() {}

  // Almacenar el Token
  setToken(token: string): void {
    localStorage.setItem(this.TOKEN_KEY, token);
  }

  // Obtener el Token
  getToken(): string | null {
    return localStorage.getItem(this.TOKEN_KEY);
  }

  // Verificar si el Token Existe
  hasToken(): boolean {
    return !!this.getToken();
  }

  // Eliminar el Token
  removeToken(): void {
    localStorage.removeItem(this.TOKEN_KEY);
  }
}

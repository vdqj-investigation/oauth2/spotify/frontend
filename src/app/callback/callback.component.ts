import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.css'],
})
export class CallbackComponent implements OnInit {
  // constructor(private route: ActivatedRoute, private router: Router) {}

  // ngOnInit() {
  //   // Extrae el código de la URL
  //   this.route.queryParams.subscribe((params) => {
  //     const code = params['code'];

  //     if (code) {
  //       console.log('Received code:', code);
  //       // Aquí podrías añadir la lógica para enviar el código a tu servidor
  //       // Y luego manejar la respuesta adecuadamente (almacenar el token, manejar errores, etc.)
  //     } else {
  //       // Manejar situaciones donde no se recibe un código (mostrar mensaje, redirigir, etc.)
  //       console.log('No hay code...');
  //     }
  //   });
  // }
  userEmail: string | undefined;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    // Ahora handleAuth se llama aquí, en el componente específico de callback
    // this.authService.handleAuth();

    const code = new URLSearchParams(window.location.search).get('code');
    if (code) {
      console.log('code: ', code);

      // Usa AuthService para manejar el intercambio de código
      this.authService.exchangeCode(code).subscribe(
        (data: any) => {
          // Manejar la respuesta, guardar tokens, redireccionar usuario, etc.
          console.log('data> ', data);
          const accessToken = data.accessToken;
          this.authService
            .getUserData(accessToken)
            .subscribe((userData: any) => {
              this.userEmail = userData.email; // Asumiendo que userData contiene el correo electrónico
              console.log('Email del usuario:', this.userEmail);
            });
        },
        (error) => {
          console.log('error', error);
          // Manejar errores aquí
        }
      );
    }
  }
}

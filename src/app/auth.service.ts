import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private clientId = '88fb9a09f0ae467982a84681b93886ae';
  private redirectUri = 'http://localhost:4200/callback';

  constructor(private router: Router, private http: HttpClient) {}

  login() {
    const scope = 'user-read-private user-read-email'; // Cambia esto según los scopes que necesites.
    window.location.href = `https://accounts.spotify.com/authorize?client_id=${
      this.clientId
    }&redirect_uri=${encodeURIComponent(
      this.redirectUri
    )}&scope=${encodeURIComponent(scope)}&response_type=code&show_dialog=true`;
  }

  handleAuth() {
    // Capturar el código de la URL aquí y luego podrías enviarlo al backend.
    const code = new URLSearchParams(window.location.search).get('code');
    if (code) {
      // Aquí enviarías el código a tu backend NestJS para intercambiarlo por un token.
      console.log('Authorization Code Victor:', code);
      // Redireccionar al usuario a otra parte de la aplicación o manejar el flujo como prefieras.
      this.router.navigate(['/']);
    }
  }

  // Método para manejar el intercambio de código
  exchangeCode(code: string) {
    const url = 'http://localhost:3000/auth/callback?code=' + code;
    return this.http.get(url); // Ajusta según sea necesario (tipo de respuesta, manejo de errores, etc.)
  }

  // Método para obtener datos del perfil del usuario
  getUserData(accessToken: string) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${accessToken}`,
    });

    return this.http.get('https://api.spotify.com/v1/me', { headers });
  }
}
